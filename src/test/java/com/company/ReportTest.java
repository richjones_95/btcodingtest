package com.company;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;


class ReportTest {

    public static String testFile = "/home/richard/Desktop/testFile.txt";
    private List<String> expectedLines = new ArrayList<>();


    @BeforeEach
    void setup() {
        expectedLines.add("1508405807242 1508405807141 vader HELLO");
        expectedLines.add("1508405807340 1508405807350 luke HELLO");
        expectedLines.add("1508405807378 1508405807387 luke LOST vader");
        expectedLines.add("1508405807467 1508405807479 luke FOUND r2d2");
        expectedLines.add("1508405807468 1508405807480 luke LOST leia");
        expectedLines.add("1508405807512 1508405807400 vader LOST luke");
        expectedLines.add("1508405807560 1508405807504 vader HELLO");
    }


    @Test
    void readFile() {
        Report report = new Report();
        List<String> recordListTest = report.readFile(testFile);
        Assertions.assertEquals(recordListTest, expectedLines);
        System.out.print(1);
    }

    @Test
    void generateReport() {
        Report reportobj = new Report();

        // Setup
        List<String> lines = reportobj.readFile(testFile);
        List<Record> report = reportobj.generateReport(lines);

        //Expected Report
        List<String> expectedReport = new ArrayList<>();
        expectedReport.add("vader ALIVE 1508405807560 vader HELLO");
        expectedReport.add("luke ALIVE 1508405807468 luke LOST leia");
        expectedReport.add("r2d2 ALIVE 1508405807467 luke FOUND r2d2");
        expectedReport.add("leia DEAD 1508405807468 luke LOST leia");


        for (int i = 0; i < report.size(); i++) {
            Assertions.assertEquals(report.get(i).toString(), expectedReport.get(i));
        }
    }
}
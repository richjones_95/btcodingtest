package com.company;


import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;


public class Report {

    private static String ALIVE = "ALIVE";
    private static String UNKNOWN = "UNKNOWN";
    private static String DEAD = "DEAD";
    // A threshold to ensure the message has propagated thought the system.
    private static int SYNC_TIME_THRESHOLD = 50;

    /**
     * Read input from a text file.
     *
     * @param fileName - name of provided text file in program args
     * @return report - return a list that contain the parsed information.
     */
    public List<String> readFile(String fileName) {
        // Contains all available
        List<String> lines = null;
        try {
            lines = Files.readAllLines(Paths.get(fileName), Charset.defaultCharset());
        } catch (IOException ioe) {
            System.err.print("Error Parsing file, is it the correct provide the correct format?  timeStampGenerated, timestampReceived, nodeName, message");
            System.exit(1);
        }
        return lines;
    }


    /**
     * Generate a report based of the message objects.
     *
     * @param lines - list of record from a input file.
     */
    public List<Record> generateReport(List<String> lines) {

        List<Record> report = new ArrayList<>();

        if (lines == null) {
            System.err.print("Cannot Read input");
            System.exit(1);
        }

        lines.forEach(line -> {
            String args[] = line.split(" ", 3);

            // Current Node | Node Status | Time Stamp Generation | Node Current name | Notification | Message
            // TimeStamp Received | TimeStamp Generated | Current Node | message
            // TimeStamp Received | TimeStamp Generated | Current Node | message (Status | Target Node)
            // Check Line Input
            if (args.length < 3 || args.length > 6) {
                System.err.print("Bad input, incorrect number of arguments should be between 4 and 5");
                System.exit(1);
            }

            // Record data
            long timestampReceived = Long.parseLong(args[0]);
            long timestampGenerated = Long.parseLong(args[1]);
            String message = args[2];
            // current node | Status | target node
            String recordMessage[] = message.split(" ");
            String currentNode = recordMessage[0];
            String status = recordMessage[1];


            // If the record message length = 1 then we just have one message.
            // So we can just add it to the record object.
            // However if record message is greater than one i.e multiple words then we need to look for node information.
            // The message is saved to the target node and it creates.
            // e.g. luke found r2d2 -> |r2d2 |ALIVE |timeStamp Received | message when discovered.
            if (recordMessage.length > 2) {
                // | STATUS | TARGET NODE
                String targetNode = recordMessage[2];

                // Check if current node has already been created.
                // Look at the object in the list and compare the node name against the args[3]
                int matchedIndex = findMatchingName(report, currentNode);

                // Does the current node exist already?
                if (matchedIndex != -1) {
                    //Do I need check here to compare time stamps.
                    // Only change timestamps if they are after the one you've already got.
                    if (timestampReceived > report.get(matchedIndex).getTimestampReceived()) {
                        report.get(matchedIndex).setStatus(ALIVE);
                        //Overwrite node with the latest data
                        report.get(matchedIndex).setTimestampReceived(timestampReceived);
                        report.get(matchedIndex).setTimeStampGenerated(timestampGenerated);
                        //Set to latest Message XXX
                        report.get(matchedIndex).setMessage(message);
                    }
                } else {
                    // Need to create a new node.
                    // But before that let do a Sync check.
                    Record record = new Record(timestampReceived, timestampGenerated, currentNode, ALIVE, message);
                    report.add(record);
                }

                //TARGET NODE
                int matchedIndexTargetNode = findMatchingName(report, targetNode);
                // Does the Target node exist already
                if (matchedIndexTargetNode != -1) {

                    // We check if the last message was less than 50 milliseconds ago.
                    if (timestampReceived - report.get(matchedIndexTargetNode).getTimestampReceived() > SYNC_TIME_THRESHOLD) {

                        //Overwrite node with the latest data
                        report.get(matchedIndexTargetNode).setTimestampReceived(timestampReceived);
                        report.get(matchedIndexTargetNode).setTimeStampGenerated(timestampGenerated);
                        //Set to latest Message
                        report.get(matchedIndexTargetNode).setMessage(message);

                        if (status.equals("FOUND")) {
                            report.get(matchedIndexTargetNode).setStatus(ALIVE);
                        } else if (status.equals("LOST")) {
                            report.get(matchedIndexTargetNode).setStatus(DEAD);
                        } else {
                            report.get(matchedIndexTargetNode).setStatus(UNKNOWN);
                        }
                    }

                } else {
                    Record record;
                    //Timestamp check here?
                    if (status.equals("FOUND")) {
                        record = new Record(timestampReceived, timestampGenerated, targetNode, ALIVE, message);
                    } else if (status.equals("LOST")) {
                        record = new Record(timestampReceived, timestampGenerated, targetNode, DEAD, message);
                    } else {
                        record = new Record(timestampReceived, timestampGenerated, targetNode, UNKNOWN, message);
                    }
                    report.add(record);
                }

            } else {
                    /*
                     NODE WITH SINGULAR MESSAGE.
                     We don't have a target node here.
                     TimeStamp Received | TimeStamp Generated | Current Node | message (Hello)
                     vader ALIVE 1508405807560 vader HELLO

                     Does the current node exist already?
                     Check if current node has already been created.
                     Look at the object in the list and compare the node name against the args[3]
                     */

                int matchedIndex = findMatchingName(report, currentNode);
                if (matchedIndex != -1) {

                    //Set to Alive because if it can send a message it must be alive.
                    report.get(matchedIndex).setStatus(ALIVE);

                    if (timestampReceived > report.get(matchedIndex).getTimestampReceived()) {
                        //Overwrite node with the latest data
                        report.get(matchedIndex).setTimestampReceived(timestampReceived);
                        report.get(matchedIndex).setTimeStampGenerated(timestampGenerated);
                        //Set to latest Message
                        report.get(matchedIndex).setMessage(message);
                    }
                } else {
                    // Create a new node
                    //Must be Alive to send a message.
                    Record record = new Record(timestampReceived, timestampGenerated, currentNode, status, message);
                    report.add(record);
                }

            }


        });
        return report;

    }

    public void printReport(List<Record> report) {
        report.forEach(System.out::println);
    }

    private int findMatchingName(List<Record> list, String nodeName) {
        int counter = 0;
        for (Record record : list) {

            if (record.getNodeName().compareTo(nodeName) == 0) {
                return counter;
            }
            counter++;
        }
        return -1;
    }
}

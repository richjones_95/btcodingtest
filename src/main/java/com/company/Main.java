package com.company;

import java.util.List;


public class Main {

    public static void main(String[] args) {

        // Validate the number of parameters
        if (args.length != 1) {
            System.err.print("Incorrect Number of arguments");
            System.exit(1);
        }

        Report reportObj = new Report();
        List<String> line = reportObj.readFile(args[0]);
        List<Record> report = reportObj.generateReport(line);
        reportObj.printReport(report);

    }


}

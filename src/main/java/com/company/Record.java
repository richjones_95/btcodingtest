package com.company;

public class Record {

    private long timeStampGenerated;
    private long timestampReceived;
    private String nodeName;
    private String message;
    private String status;

    public Record(long timestampReceived, long timeStampGenerated, String nodeName, String status, String message) {
        this.timeStampGenerated = timeStampGenerated;
        this.timestampReceived = timestampReceived;
        this.nodeName = nodeName;
        this.status = status;
        this.message = message;
    }

    public long getTimeStampGenerated() {
        return timeStampGenerated;
    }

    public void setTimeStampGenerated(long timeStampGenerated) {
        this.timeStampGenerated = timeStampGenerated;
    }

    public long getTimestampReceived() {
        return timestampReceived;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setTimestampReceived(long timestampReceived) {
        this.timestampReceived = timestampReceived;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return nodeName + ' ' + status + ' ' + timestampReceived + ' ' + message;
    }
}
